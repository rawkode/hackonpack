use actix::prelude::*;

use crate::cluster::Controller as ClusterController;

mod prometheus;
pub use prometheus::Controller as PrometheusController;

pub trait MetricController: Actor {
    fn new(
        cluster_controller: Addr<ClusterController>,
        metric: String,
        labels: Vec<String>,
        over: u64,
        under: u64,
    ) -> Self;

    fn check_for_alert(act: &mut Self, ctx: &mut Self::Context) -> ();
}
