use actix::prelude::*;
use proq::prelude::*;
use std::time::Duration;

use super::MetricController;
use crate::cluster::Controller as ClusterController;
use crate::cluster::ScaleUpCommand;

pub struct Controller {
    cluster_controller: Addr<ClusterController>,
    client: ProqClient,
    metric: String,
    labels: Vec<String>,
}

impl Controller {
    pub fn create(cluster_controller: Addr<ClusterController>) -> Addr<Self> {
        return Controller {
            cluster_controller: cluster_controller,
            client: ProqClient::new("localhost:9090", Some(Duration::from_secs(5))).unwrap(),
            metric: String::from("latency"),
            labels: vec![String::from("node=abc123")],
        }
        .start();
    }
}

impl Actor for Controller {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        info!("Starting ...");

        ctx.run_interval(Duration::from_secs(5), Self::check_for_alert);
    }
}

impl MetricController for Controller {
    fn new(
        cluster_controller: Addr<crate::cluster::Controller>,
        metric: String,
        labels: Vec<String>,
        over: u64,
        under: u64,
    ) -> Self {
        todo!()
    }

    fn check_for_alert(act: &mut Controller, ctx: &mut Context<Controller>) -> () {
        info!("Controller Loop");
        info!("Checking if {} with labels {:?}", act.metric, act.labels);

        // futures::executor::block_on(async {
        //     let end = Utc::now();
        //     let start = Some(end - chrono::Duration::minutes(1));
        //     let step = Some(Duration::from_secs_f64(1.5));
        //     let rangeq = act
        //         .client
        //         .range_query("go_goroutines", start, Some(end), step)
        //         .await;
        // });

        act.cluster_controller.do_send(ScaleUpCommand);
        return ();
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct ReloadCommand;

impl Handler<ReloadCommand> for Controller {
    type Result = ();

    fn handle(&mut self, _: ReloadCommand, _ctx: &mut Self::Context) -> Self::Result {
        error!("Howdy. Reloading");
    }
}
