use actix::prelude::*;
use clap::arg_enum;
use parse_duration::parse;
use proq::prelude::*;
use std::time::Duration;
use structopt::StructOpt;

#[macro_use]
extern crate log;

mod gitops;
use gitops::GitSync;

mod cluster;
use cluster::Controller as ClusterController;

mod metrics;
use metrics::PrometheusController;

arg_enum! {
    #[derive(Debug)]
    enum LogLevel {
        Error,
        Debug,
        Warn,
        Info,
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "jetpack")]
/// GitOps AutoScaler for Packet's Bare Metal Cloud
struct Opt {
    // Debug Mode
    #[structopt(short, default_value = "warn", possible_values = &LogLevel::variants(), case_insensitive = true)]
    log_level: LogLevel,

    /// Url for the Git repository. Authentication not supported currently
    #[structopt(short = "r", long = "repository")]
    git_url: String,

    /// How frequently to pull updates from the remote (1h, 1d, 1d5h, 10s)
    #[structopt(short = "s", long, default_value = "1h", parse(try_from_str = parse))]
    sync_every: Duration,
}

fn main() {
    let opt = Opt::from_args();

    std::env::set_var("RUST_LOG", format!("jetpack={}", opt.log_level));
    env_logger::init();

    let system = actix::System::new("jetpack");

    let cluster_controller = ClusterController {}.start();

    // Start our GitSync Process
    GitSync {
        git_url: opt.git_url,
        sync_every: opt.sync_every,
        cluster_controller: cluster_controller.clone(),
    }
    .start();

    let prom_addr = PrometheusController::create(cluster_controller.clone());

    system.run();
}
