// Ignoring Storage for now, as this isn't configurable. However, we may wish to incorporate this into the metadata for other purposes later.
pub struct Metadata {
    id: String,
    hostname: String,
    iqn: String,
    operating_system: OperatingSystem,
    plan: String,
    class: String,
    facility: String,
    ssh_keys: Vec<String>,
    tags: Vec<String>,
    networks: Vec<Network>,
    userdata: String,
}
pub struct OperatingSystem {
    slug: String,
    distro: String,
    version: String,
    image_tag: String,
}

pub enum Version {
    IPv4,
    IPv6,
}

pub struct Network {
    id: String,
    version: Version,
    subnet: u16,
}
