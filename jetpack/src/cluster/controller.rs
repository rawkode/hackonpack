use actix::prelude::*;
use std::time::Duration;

pub struct Controller {}

impl Actor for Controller {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        info!("Starting ...");

        ctx.run_interval(Duration::from_secs(5), move |act, ctx| {
            info!("Controller Loop");
        });
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct ReloadCommand;

impl Handler<ReloadCommand> for Controller {
    type Result = ();

    fn handle(&mut self, _: ReloadCommand, _ctx: &mut Self::Context) -> Self::Result {
        error!("Howdy. Reloading");
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct ScaleUpCommand;

impl Handler<ScaleUpCommand> for Controller {
    type Result = ();

    fn handle(&mut self, _: ScaleUpCommand, _ctx: &mut Self::Context) -> Self::Result {
        error!("Howdy. ScaleUp");
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct ScaleDownCommand;

impl Handler<ScaleDownCommand> for Controller {
    type Result = ();

    fn handle(&mut self, _: ScaleDownCommand, _ctx: &mut Self::Context) -> Self::Result {
        error!("Howdy. ScaleDown");
    }
}
